from flask import render_template, flash, redirect, url_for, request
from flask_login import login_user, current_user, logout_user, login_required
from app import app, db
from app.forms import LoginForm, BahnhofForm, RegisterForm
from app.models import User, Bahnhof
from sqlalchemy import select

@app.route("/get_user", methods=["GET"])
def get_users():
    users = db.session.execute(select(User.username)).all()
    return users.__repr__()

@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User registered!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/bahnhof', methods=['GET', 'POST'])
def create_bahnhof():
    form = BahnhofForm()

    if form.validate_on_submit():
        bahnhof = Bahnhof(name=form.name.data, adress=form.adress.data)
        db.session.add(bahnhof)
        db.session.commit()
        return redirect(url_for('index'))

    return render_template('bahnhof.html', title='Bahnhof', form=form)


@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'florian'}
    #gruß = [
            #{'bahnhof': {'username': 'Linz HBF'},
              #  'adress': 'Bahnhofstrasse 3-6'
           # },'''
    #]
    return render_template('index.html', title='Home', user=user) #fakeEintraege = gruß)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page:
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Log In', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/user/<username>')
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    bahnhoefe = [
        {'name': {'name': 'Linz HBF'},
         'adress': 'Bahnhofstrasse 3-6'
         },
        {'name': {'name': 'Wien WBF'},
         'adress': 'Westbahnhhof 1-3'
         }
    ]
    return render_template('user.html', user=user , bahnhof=bahnhoefe)
